import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DownloadService {

  courseTar: Observable<Blob>;
  releaseUrl: string = 'https://api.github.com/repos/bogach/prof-courses/releases/latest'

  constructor(private http: HttpClient) { }

  getLastCoursesUrl(): Observable<Blob> {
    this.http.get(this.releaseUrl)
      .subscribe((data: any) => {
        // this.donwloadCourses(data.tarball_url);
      });
    return this.courseTar;
  }

  donwloadCourses(): Observable<Blob> {
    // this.courseTar = this.http.get<Blob>(url);
    return this.http.get<Blob>("https://api.github.com/repos/bogach/prof-courses/tarball/1.2.3");
  }
}
