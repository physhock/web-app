/// <reference lib="webworker" />

import { PitcherDetector } from './dspcore-wasm.module.js'

let dspcore: any = PitcherDetector;

addEventListener('message', ({ data: audioData }) => {
  let signalVector = dspcore.arrayToVector(audioData);
  let result = dspcore.detectPitch(
    signalVector, // samples
    4100,         // sampleRate
    1024,         // frameSize
    0,            // algorithm ( enum:0 -- YIN )
  );
  
  result.smoothedSamples = dspcore.normalize(result.smoothedSamples);
  result.timeStamps =  dspcore.genTimestamps(result.smoothedSampleRate, result.smoothedSamples.size());
  result.timeStamps = dspcore.normalize(result.timeStamps);

  result.smoothedSamples = dspcore.vectorToArray(result.smoothedSamples);
  result.timeStamps = dspcore.vectorToArray(result.timeStamps);
  postMessage(result);
});