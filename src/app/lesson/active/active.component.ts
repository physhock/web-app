import { Component, OnInit } from '@angular/core';
import { DownloadService } from 'src/app/services/download.service';

@Component({
  selector: 'app-active',
  templateUrl: './active.component.html',
  styleUrls: ['./active.component.css']
})
export class ActiveComponent implements OnInit {

  clickMessage: string;

  audioURL: string;
  mediaRecorder: MediaRecorder;
  audioContext: AudioContext = new AudioContext();

  plotly: any;
  worker: Worker;

  constructor(private downloadService: DownloadService) { }

  ngOnInit(): void {
    this.plotly = window['Plotly'];
    this.worker = new Worker('../../services/dsp/essentia.worker', { type: 'module' })

    // this.downloadService.donwloadCourses().subscribe((tar: Blob) => {

    //   console.log(tar);

    // }
    // );

    this.worker.onmessage = ({ data }) => {
      console.log(data);
      this.create(data);
    };


    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
      this.mediaRecorder = new MediaRecorder(stream);
      let chunks = [];
      this.mediaRecorder.ondataavailable = (event) => chunks.push(event.data);
      this.mediaRecorder.onstop = () => {
        console.log(this.audioContext.sampleRate);
        const blob = new Blob(chunks, { 'type': 'audio/ogg; codecs=opus' });
        chunks = [];
        this.getAudioChannelDataFromURL(blob).then(array => this.worker.postMessage(array));
      };
    });
  }

  async getAudioChannelDataFromURL(audioBlob: Blob, channel: number = 0) {
    const arrayBuffer = await audioBlob['arrayBuffer']();
    const audioBuffer = await this.audioContext.decodeAudioData(arrayBuffer);
    return audioBuffer.getChannelData(channel);
  }

  makeLinearSpace(start: any, stop: any, num: any) {
    if (typeof num === "undefined") num = Math.max(Math.round(stop - start) + 1, 1);
    if (num < 2) { return num === 1 ? [start] : []; }
    var i, ret = Array(num);
    num--;
    for (i = num; i >= 0; i--) { ret[i] = (i * stop + (num - i) * start) / num; }
    return ret;
  }

  create(data: any) {

    let LayoutMelodyContourPlot = {
      title: "Melody Contour",
      plot_bgcolor: "transparent",
      paper_bgcolor: "#FCF7F7",
      autosize: false,
      width: 670,
      height: 300,
      xaxis: {
        type: "time",
        title: "Time"
      },
      yaxis: {
        autorange: true,
        type: "linear",
        title: "Frequency (Hz)"
      }
    }

    LayoutMelodyContourPlot.title = "PitchYA";


    // Create a plotly plot instance if a plot hasn't been created before
    this.plotly.newPlot('plotDiv', [{
      x: data.timeStamps,
      y: data.smoothedSamples,
      mode: 'lines',
      line: { color: '#2B6FAC', width: 2 }
    }], LayoutMelodyContourPlot);
  }

  listenAudio() {
    // let a = new Audio(this.audioURL);
    // a.play();
    // this.clickMessage = "playing";
    // this.worker.postMessage("kek");
  }

  startRecording() {
    if (this.mediaRecorder.state === "recording") {
      this.mediaRecorder.stop();
    }
    else {
      this.mediaRecorder.start();
    }
    this.clickMessage = this.mediaRecorder.state;
  }

  clearRecord() {
    this.clickMessage = "clearing";
  }
}
