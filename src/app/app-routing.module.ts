import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActiveComponent } from './lesson/active/active.component';
import { ListComponent as CourseList } from './course/list/list.component'
import { ListComponent as LessonList } from './lesson/list/list.component';


const routes: Routes = [
  { path: '', component: ActiveComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
